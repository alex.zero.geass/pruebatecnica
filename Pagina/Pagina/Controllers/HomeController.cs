﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pagina.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult Login(string user, string pass)
        {
            string respuesta;
            try
            {
                if (!string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(pass))
                {
                    respuesta = "Conectariamos con el servidor.";
                }
                else
                {
                    respuesta = "Usuario o Contraseña vacios";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }
    }
}