SELECT
  p.NOMBRE,
  pr.DESCRIPCION
FROM PROYECTO p
INNER JOIN PRODUCTO_PROYECTO prp ON p.PROYECTO = prp.PROYECTO
INNER JOIN PRODUCTO pr ON p.PRODUCTO = pr.PRODUCTO
WHERE
  p.PRODUCTO = 1;

SELECT
  m.COD_MENSAJE,
  p.NOMBRE,
  pr.DESCRIPCION
FROM MENSAJE m
INNER JOIN PROYECTO p ON m.PROYECTO = p.PROYECTO
INNER JOIN PRODUCTO pr ON m.PRODUCTO = pr.PRODUCTO;

SELECT
  m.COD_MENSAJE,
  p.NOMBRE,
  COALESCE(
    GROUP_CONCAT(pr.DESCRIPCION, ','),
    'TODOS'
  ) AS PRODUCTOS
FROM MENSAJE m 
LEFT JOIN PROYECTO p ON m.PROYECTO = p.PROYECTO
LEFT JOIN PRODUCTO pr ON m.PRODUCTO = pr.PRODUCTO
GROUP BY
  m.COD_MENSAJE,
  p.NOMBRE;