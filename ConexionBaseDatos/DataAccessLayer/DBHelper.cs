﻿using MySql.Data.MySqlClient;
using System.Data;

namespace DataAccessLayer
{
    public class DBHelper
    {
        private string con;

        public DBHelper(string con)
        {
            this.con = con;
        }

        public DataSet ExecuteSelectQuery(string query)
        {
            using (MySqlConnection connection = new MySqlConnection(con))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);

                    return dataSet;
                }
            }
        }

        public int ExecuteNonQuery(string query)
        {
            using (MySqlConnection connection = new MySqlConnection(con))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    return command.ExecuteNonQuery();
                }
            }
        }

    }
}
