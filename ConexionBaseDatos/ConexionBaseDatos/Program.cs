﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConexionBaseDatos
{
    class Program
    {
        static void Main(string[] args)
        {
            //Para que no sea tan largo coloco la cadena de conexion aca aunque por temas de seguridad no deberia
            string connection = "server=bcn8lajevwyvx7uxdxq1-mysql.services.clever-cloud.com;database=bcn8lajevwyvx7uxdxq1;user=uplywwqc5mf7j5cd;password=7Bk2WpFiHs5VlOHoWtnV;port=3306";
            DBHelper dbHelper = new DBHelper(connection);

            string selectQuery = "SELECT * FROM tabla_prueba_tecnica";
            DataSet resultDataSet = dbHelper.ExecuteSelectQuery(selectQuery);

            //1. Obtiene la cantidad de filas afectadas en el insert
            string insertQuery = "INSERT INTO tabla_prueba_tecnica (Nombre, Correo) VALUES ('Usuario" + resultDataSet.Tables[0].Rows.Count+ "', 'Valor" + resultDataSet.Tables[0].Rows.Count + "@valor.com')";
            int rowsInserted = dbHelper.ExecuteNonQuery(insertQuery);

            //2. Obtiene el dataset de un select despues de insertado el dato
             selectQuery = "SELECT * FROM tabla_prueba_tecnica";
             resultDataSet = dbHelper.ExecuteSelectQuery(selectQuery);

            //3. obtiene la cantidad de datos obtenidos en el select
            int rowCount = resultDataSet.Tables[0].Rows.Count;

            Console.WriteLine($"Número de filas devueltas por la consulta SELECT: {rowCount}");
            Console.WriteLine($"Número de filas afectadas por la consulta INSERT: {rowsInserted}");
            Console.WriteLine($"Nombre   | Correo");

            foreach (DataRow row in resultDataSet.Tables[0].Rows)
            {
                Console.WriteLine($"{row["Nombre"]} {row["Correo"]}");
            }
            Console.ReadKey();
        }
    }
}
